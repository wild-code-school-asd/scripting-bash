#!/bin/bash

read -p "Enter the name of the service you want to check: " service_name

if sudo systemctl is-active --quiet "$service_name"; then
    echo "$service_name is running."
else
    echo "$service_name is not running."
fi
