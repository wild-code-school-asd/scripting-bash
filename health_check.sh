#!/bin/bash

# Function to ping a server
ping_server() {
    local server=$1
    echo "Pinging $server..."
    sudo ping -c 1 $server > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "$server is accessible."
    else
        echo "$server is not accessible."
    fi
}

# Function to check if a port is open on a server
check_port() {
    local server=$1
    local port=$2
    echo "Checking port $port on $server..."
    nc -z $server $port > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "Port $port on $server is open."
    else
        echo "Port $port on $server is closed."
    fi
}

# Function to make an HTTP request and check the response
check_http() {
    local url=$1
    echo "Checking URL $url..."
    response=$(curl -s -o /dev/null -w "%{http_code}" $url)
    if [ $response -eq 200 ]; then
        echo "$url is accessible and returns a 200 code."
    else
        echo "$url returned response code $response."
    fi
}

# List of servers and ports to check
declare -A servers=( ["127.0.0.1"]="80" ["127.0.0.1"]="443" )

# Loop to check each server
for server in "${!servers[@]}"; do
    ping_server "$server"
    check_port "$server" "${servers[$server]}"
    check_http "http://$server"
done