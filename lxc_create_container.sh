#!/bin/bash

echo '##############################################'
echo ' Create Proxmox LXC Container                 '
echo ' https://pve.proxmox.com/pve-docs/pct.1.html  '
echo '##############################################'

# Variables 

## Id of container
container_id="128"

## Container template to use
container_template="local-hdd-templates:vztmpl/debian-12-standard_12.0-1_amd64.tar.zst"

## Hostname of the container
container_hostname="test-create-bash"

## Storage of the container
container_storage="local-hdd-datas"

## Number of CPU for the container
container_cpu="1"

## Memory (MB) for the container
container_ram="1024"

## Swap of the container (MB)
container_swap="512"

## Disk size allocation for the container (GB)
container_disk_size="4"

## IP4 address of the container (192.168.1.0/24)
container_ip4="192.168.1.128/24"

## Gateway ip4 for the container (default: 192.168.1.254)
container_gateway="192.168.1.254"

## Network bridge (vmbr2|vmbr3)
bridge="vmbr2"

# Create the container

pct create $container_id $container_template \
    -hostname $container_hostname \
    -cores $container_cpu \
    -memory $container_ram \
    -swap $container_swap \
    -storage $container_storage \
    -password \
    -net0 name=eth0,bridge=$bridge,gw=$container_gateway,ip=$container_ip4 &&\

pct start $container_id

